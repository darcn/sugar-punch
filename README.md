# Sugar Punch

Modern, Cyberpunki-sh theme that uses delightful gradients, based on Materia

This theme's design as of right now it's being currently heavily developed, and as such i only provide a base theme and a gtk.css
file which provides a preview of how the theme looks in a linux distro.

## Installation
1.  Clone this repository
2.  Extract the folder ".config" in your home folder and make sure you don't have a gtk.css file already existing if so make a backup of that file
3.  Extract the Sugar-Punch folder in ~/.themes, if you don't have that folder created then do so.

  
## Uninstallation
This theme uses a gtk.css in ~/.config/gtk-3.0/gtk.css and as such the code in it's contents will overwrite any theme in the user's pc in a temporary way
So if you don't want it to do that, you can uninstall the theme by following these instructions:

1.  Erase the following file or it's contents ~/.config/gtk-3.0/gtk.css and restore any previous backup if you made one
2.  Change your theme in the preferences of your DE

    Optional. If you want you can erase the theme's base folder in ~/.themes/Sugar-Punch

## Important
When i and everyone who gives feedback about the "theme" are happy with it's design i'll make it a real theme, erasing the ~/.config/gtk-3.0/gtk.css